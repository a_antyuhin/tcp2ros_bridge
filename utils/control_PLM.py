#!/usr/bin/python3


import socket
import time

code_command = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18,
                20, 21, 22, 23, 24, 25]

RESERVE_BYTE = 0xFF

# batch template according to the protocol
def make_batch(type_batch, type_device, command):
    batch = [type_batch, RESERVE_BYTE, type_device, RESERVE_BYTE, command, RESERVE_BYTE]
    return bytes(batch)

def send_batch(type_batch, type_device, command, test_socket):
        test_socket.send(make_batch(type_batch, type_device, command))
        response = test_socket.recv(1024)
        print('\033[32m\033[1m\033[40mReceive: ', response, '\033[0m\n')

def send_command(command, test_socket):
    # commands
    if command == 1:
        send_batch(0x02, 0x10, 0xa1, test_socket)
    if command == 2:
        send_batch(0x02, 0x10, 0xa2, test_socket)
    if command == 3:
        send_batch(0x02, 0x20, 0xa1, test_socket)
    if command == 4:
        send_batch(0x02, 0x20, 0xa2, test_socket)
    if command == 5:
        send_batch(0x02, 0x30, 0xb1, test_socket)
    if command == 6:
        send_batch(0x02, 0x30, 0xb2, test_socket)
    if command == 7:
        send_batch(0x02, 0x30, 0xb3, test_socket)
    if command == 8:
        send_batch(0x02, 0x30, 0xb4, test_socket)
    if command == 9:
        send_batch(0x02, 0x30, 0xb5, test_socket)
    if command == 10:
        send_batch(0x02, 0x30, 0xb6, test_socket)
    if command == 11:
        send_batch(0x02, 0x30, 0xb7, test_socket)
    if command == 12:
        send_batch(0x02, 0x30, 0xb8, test_socket)
    if command == 13:
        send_batch(0x02, 0x40, 0xc1, test_socket)
    if command == 14:
        send_batch(0x02, 0x40, 0xc2, test_socket)
    if command == 15:
        send_batch(0x02, 0x50, 0xc1, test_socket)
    if command == 16:
        send_batch(0x02, 0x50, 0xc2, test_socket)
    if command == 17:
        test_socket.send(make_batch(0x02, 0x10, 0xa1))
        resp_1 = test_socket.recv(1024)
        test_socket.send(make_batch(0x02, 0x20, 0xa1))
        resp_2 = test_socket.recv(1024)
    if command == 18:
        test_socket.send(make_batch(0x02, 0x10, 0xa2))
        resp_1 = test_socket.recv(1024)
        test_socket.send(make_batch(0x02, 0x20, 0xa2))
        resp_2 = test_socket.recv(1024)
    

    # requests
    if command == 20:
        send_batch(0x01, 0x10, 0xa0, test_socket)
    if command == 21:
        send_batch(0x01, 0x20, 0xa0, test_socket)
    if command == 22:
        send_batch(0x01, 0x30, 0xb0, test_socket)
    if command == 23:
        send_batch(0x01, 0x40, 0xc0, test_socket)
    if command == 24:
        send_batch(0x01, 0x50, 0xc0, test_socket)
    if command == 25:
        test_socket.send(bytes('INVALID_PACKAGE', encoding='utf-8'))
        response = test_socket.recv(1024)
        print('Receive: ', response)


if __name__ == '__main__':
    test_socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)

    test_socket.connect(('169.254.61.200', 4444))  # module control ip - 10.0.0.10
    print('\033[32m\033[1mSuccessful connection...\033[0m')

    while True:
        print('\033[33m\033[1mPlease, enter code command:\033[0m')
        command = int(input())
        if code_command.count(command):
            send_command(command, test_socket)
        else:
            print('\033[31mInvalid command. Please enter correct code command!\033[0m')