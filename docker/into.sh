#!/bin/bash

ARCH=`uname -m`

docker exec --user "docker_tcp_bridge" -it tcp_bridge \
        /bin/bash -c ". /ros_entrypoint.sh; cd /home/docker_tcp_bridge/catkin_ws; /bin/bash"
