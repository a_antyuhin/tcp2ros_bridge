#!/bin/bash

ARCH=`uname -m`

docker exec --user "docker_tcp_bridge" -it tcp_bridge_ \
        /bin/bash -c ". /ros_entrypoint.sh; cd /home/docker_tcp_bridge/catkin_ws; source devel/setup.bash; roslaunch tcp_bridge tcp_bridge_start.launch; /bin/bash"
