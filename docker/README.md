Создание образа из докер файла:
```
./build.sh
```

Запуск контейнера:
```
./start.sh
```

Подключение к контейнеру:
```
./into.sh
```

Сборка компонента для отладки:
```
./src/build_component.sh
source devel/setup.bash
roslaunch tcp_bridge tcp_bridge_start.launch
```

Сборка компонента для релиза:
```
cd catkin_ws 
catkin_make install -DCMAKE_BUILD_TYPE=Release --only-pkg-with-deps tcp_bridge
source install/setup.bash
rm -R build devel
```

После того как компоненты установлены можно закоммитить контейнер:
```
docker commit <container_name> <ARCH>noetic/tcp_bridge:release