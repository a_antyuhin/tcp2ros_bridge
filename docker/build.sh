#!/bin/bash

orange=`tput setaf 3`
reset_color=`tput sgr0`

ARCH=`uname -m`

# echo "Build requires GIT user:password"
# read -p "${orange}Enter git user name: " GIT_USER;
# read -s -p "${orange}Enter git user password: ${reset_color}" GIT_PASSWD;
# echo ""

echo "Building for ${orange}${ARCH}${reset_color}"

docker build .. \
    -f Dockerfile.${ARCH} \
    --build-arg UID=$(id -g) \
    --build-arg GID=$(id -g) \
    --build-arg GIT_USER=${GIT_USER} \
    --build-arg GIT_PASSWD=${GIT_PASSWD} \
    -t ${ARCH}noetic/tcp_bridge:latest
