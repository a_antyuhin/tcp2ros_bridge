#!/usr/bin/python3


import socket
import rospy

from tcp_bridge.msg import Protocol

RESERVE_BYTE = 0xFF

# batch template according to the protocol
def make_batch (type_batch, type_device, command):
    batch = [type_batch, RESERVE_BYTE, type_device, RESERVE_BYTE, command, RESERVE_BYTE]
    return bytes(batch)

# wrapping a message according to the protocol and sending it
def callback(data):
    try:
        message = make_batch(data.type_batch, data.type_device, data.command)
        bridge_socket.send(message)
        response = bridge_socket.recv(1024)
        response_list = list(response)
        bridge.publish_response(response_list)
    except BrokenPipeError as err:
        rospy.logerr('Connection to the PLM is lost')
    except ConnectionRefusedError as err:
        rospy.logerr('ConnectionError: %s', err)
    except ConnectionResetError as err:
        rospy.logerr('ConnectionResetError: %s', err)
    except KeyboardInterrupt:
        rospy.loginfo('Bridge was stopped')
        bridge_socket.close()


class Bridge():
    def __init__(self):
        sub = rospy.Subscriber("request_to_PLM", Protocol, callback)
    
    pub = rospy.Publisher("response_from_PLM", Protocol, queue_size=10)
    msg = Protocol()
    
    def publish_response(self, response):
        self.msg.header.frame_id = 'payload_module'
        self.msg.header.stamp = rospy.get_rostime()
        if response[0] == 0x03 or response[0] == 0x04:
            self.msg.type_batch = response[0]
            self.msg.type_device = response[2]
            self.msg.command = response[4]
            self.pub.publish(self.msg)
        else:
            self.pub.publish(self.msg)

if __name__ == '__main__':
    rospy.init_node('bridge')
    bridge_socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)

    ip_address = rospy.get_param('~ip', '127.0.0.1')
    port = rospy.get_param('~port', 9999)
    
    try:
        bridge = Bridge()
        rospy.loginfo('Bridge is running...')
        
        bridge_socket.connect((ip_address, port)) # module control ip - 10.0.0.10
        rospy.loginfo('Successful connection...')

        rospy.spin()
    except ConnectionRefusedError as err:
        rospy.logerr('ConnectionError: %s', err)
    except rospy.ROSInterruptException: 
        rospy.loginfo('ROSInterrupt exception')
    except ConnectionResetError as err:
        rospy.logerr('ConnectionResetError: %s', err)
    except KeyboardInterrupt:
        rospy.loginfo('Bridge was stopped')
    finally:
        bridge_socket.close()